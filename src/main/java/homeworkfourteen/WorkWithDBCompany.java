package homeworkfourteen;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import java.sql.*;
import java.text.ParseException;

/**
 * create new table with for users in my data base 'company'
 */
public class WorkWithDBCompany {

    /**
     * driver for mysql
     */
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";

    /**
     * url of my data base
     */
    private static final String URL = "jdbc:mysql://localhost:3306/company";

    /**
     * name of user
     */
    private static final String USER = "root";

    /**
     * users's password
     */
    private static final String PASSWORD = "admin";

    /**
     * data of new users
     */
    private static final String[][] users = {{"Ivan", "johnnykutcher87@gmail.com", "1234"},
            {"Alex", "alex89@gmail.com", "5678"},
            {"Mitya", "mityok@ukr.net", "9156"},
            {"Nata", "zbanackayanata@yahoo.com", "3335"},
            {"Ivan", "johnnykutcher87@gmail.com", "1234"},
            {"Oleg", "oleg17@gmail.com", "8621"}};

    /**
     * main method
     * @param args
     */
    public static void main(String[] args) {
        try {
            WorkWithDBCompany.getDBConnection();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * method for creating connection
     * @return
     * @throws ParseException
     */
    private static Connection getDBConnection() throws ParseException {
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
            createDBUserTable(connection);
            insertDataIntoDB(connection, users);
            return connection;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * method for creating a table in data base
     * @param connection
     * @throws SQLException
     * @throws ParseException
     */
    public static void createDBUserTable(Connection connection) throws SQLException, ParseException {

        String createTableSQL = "CREATE TABLE IF NOT EXISTS USER(ID INT PRIMARY KEY AUTO_INCREMENT,"
                + "NAME VARCHAR(20) NOT NULL,"
                + "LOGIN VARCHAR(35) NOT NULL UNIQUE,"
                + "PASSWORD VARCHAR(8)"
                + ") ENGINE InnoDB";

        try (Statement statement = connection.createStatement()) {
            statement.execute(createTableSQL);



            System.out.println("Table  \"user\" is created!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * method for inserting new field in data base's table
     * @param connection
     * @param users
     * @throws ParseException
     */
    private static void insertDataIntoDB(Connection connection, String[][] users) throws ParseException {
        String insertTableSQL = "INSERT INTO USER"
                + "(NAME, LOGIN, PASSWORD) " + "VALUES"
                + "(?, ?, ?)";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insertTableSQL);
            for (int i = 0; i < users.length; i++) {
                    preparedStatement.setString(1, String.valueOf(users[i][0]));
                    preparedStatement.setString(2, String.valueOf(users[i][1]));
                    preparedStatement.setString(3, String.valueOf(users[i][2]));
                    preparedStatement.executeUpdate();

            }
        } catch (MySQLIntegrityConstraintViolationException ex) {
            System.err.println("You have entered a duplicate login");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

