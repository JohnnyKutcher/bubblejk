package jdbc;

import java.sql.*;
import java.text.ParseException;

public class DataBaseExample {

    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/school";
    private static final String USER = "root";
    private static final String PASSWORD= "admin";

    public static void main(String[] args) {
        try {
            DataBaseExample.getDBConnection();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static Connection getDBConnection() throws ParseException {
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
            createDBUserTable(connection);
            return connection;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static void createDBUserTable(Connection connection) throws SQLException, ParseException {

        String createTableSQL = "CREATE TABLE DBUSER(USER_ID INT(5) NOT NULL,"
                + "USERNAME VARCHAR(20) NOT NULL,"
                + "CREATED_BY VARCHAR(20) NOT NULL,"
                + "PRIMARY KEY (USER_ID) "
                + ")";

        try(Statement statement = connection.createStatement()) {
//            statement.execute(createTableSQL);
//            insertDataIntoDB(statement);

//            updateDataInTable(statement);
//            selectFromTable(statement);
              deletedFromTable(statement);


            System.out.println("Table  \"dbuser\" is created!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void insertDataIntoDB(Statement statement) throws ParseException {
        String insertTableSQL = "INSERT INTO DBUSER" +
                "(USER_ID, USERNAME, CREATED_BY) " + "VALUES" +
                "(1, 'ivan', 'system')";

        try {
            statement.executeUpdate(insertTableSQL);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void selectFromTable(Statement statement) {
        String selectTableSQL = "SELECT USER_ID, USERNAME from DBUSER";

        try {
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                String userID = rs.getString("USER_ID");
                String userName = rs.getString("USERNAME");

                System.out.println("userId : " + userID);
                System.out.println("username : " + userName);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void updateDataInTable(Statement statement) {
        String updateTableSQL = "UPDATE DBUSER SET USERNAME = 'ivan_new' WHERE USER_ID = 1";

        try {
            statement.execute(updateTableSQL);
            System.out.println("Record is update to DBUSER table!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void deletedFromTable(Statement statement) {
        String deleteFromTableSQL = "DELETE FROM DBUSER WHERE USER_ID = 1";

        try {
            statement.execute(deleteFromTableSQL);
            System.out.println("Record is deleted from DBUSER table!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


}
