package homeworkten;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Admin on 04.08.2017.
 */
public class CsvFileWriter {

    /**
     * method for creating empty CSV file with NIO tools
     * @param pathToFile
     * @param fileName
     */
    public static void createCSVfile(String pathToFile, String fileName) {
        try {
            //        Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rwxrwx--x");
//        FileAttribute<Set<PosixFilePermission>> attribute = PosixFilePermissions.asFileAttribute(perms);
            Path path = Paths.get(pathToFile,  fileName);
            Files.createFile(path);

            System.out.println(fileName + " was created successfully!!!");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * method for filling the file with information in this form - id, name, age
     * @param fileName
     * @param personList
     */
    public static void writeCsvFile(String fileName, List<Person> personList) {
        final String COMMA_DELIMITER = ",";
        final String NEW_LINE_SEPARATOR = "\n";

        final String FILE_HEADER = "id, name, age";

        try (FileWriter fw = new FileWriter(fileName)) {
            fw.append(FILE_HEADER.toString());

            fw.append(NEW_LINE_SEPARATOR);

            for (Person person: personList) {
                fw.append(String.valueOf(person.getId()));
                fw.append(COMMA_DELIMITER);
                fw.append(person.getName());
                fw.append(COMMA_DELIMITER);
                fw.append(String.valueOf(person.getAge()));
                fw.append(NEW_LINE_SEPARATOR);
            }

            System.out.println("CSV file was created successfully!!!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
