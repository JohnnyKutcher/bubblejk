package homeworkten;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Admin on 03.08.2017.
 */
public class FromFileToObject {

    /**
     * method for converting from CSV file to collection
     * @param fileName
     * @return
     */
    public static List<String> fromCsvFileToCollection(String fileName) {
        List<String> list = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            list = stream.collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * method for converting from Collection to User object
     * @param list
     */
    public static void fromCollectionToObject(List<String> list) {
        Pattern pattern = Pattern.compile(",");
        List<User> list1 = list.stream().skip(1).map(line -> {
            String[] x = pattern.split(line);
            return new User(Integer.parseInt(x[0]), x[1], Integer.parseInt(x[2]));
        }).collect(Collectors.toList());
        list1.forEach(System.out::println);
    }

}
