package homeworkten;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by Admin on 03.08.2017.
 */
public class CountingOfWords {

    /**
     * method for counting the number of words in a file
     * @param filePath
     * @return
     */
    public static Long countWordsFromFile(String filePath) {
        Long count = 0L;
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            count = stream.flatMap(line -> Arrays.stream(line.split("\\s"))).count();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }
}
