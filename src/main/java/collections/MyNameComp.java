package collections;

import java.util.Comparator;

/**
 * Created by Admin on 11.07.2017.
 */
public class MyNameComp implements Comparator<Empl> {

    /**
     *
     * @param oOne
     * @param oTwo
     * @return
     */
    @Override
    public int compare(Empl oOne, Empl oTwo) {
        return oOne.getName().compareTo(oTwo.getName());
    }
}
