package collections;

import java.util.Comparator;

/**
 * Created by Admin on 11.07.2017.
 */
public class MySalaryComp implements Comparator<Empl> {

    /**
     *
     * @param oOne
     * @param oTwo
     * @return
     */
    @Override
    public int compare(Empl oOne, Empl oTwo) {
        if (oOne.getSalary() > oTwo.getSalary()) {
            return 1;
        } else {
            return -1;
        }
    }
}
