package regex;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin on 19.06.2017.
 */
public class Regex {
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Pattern pt = Pattern.compile("\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,4})+");
        Matcher m = pt.matcher("johnnykutcher87@gmail.com");
        boolean ft = m.matches();

        System.out.println("ft = " + ft);

    }
}
