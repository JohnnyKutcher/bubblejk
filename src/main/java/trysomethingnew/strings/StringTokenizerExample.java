package trysomethingnew.strings;

import java.util.StringTokenizer;

/**
 * Created by Admin on 14.06.2017.
 */
public class StringTokenizerExample {
    /**
     *
     * @param str
     */
    public static void splitbySpace(String str) {
        StringTokenizer st = new StringTokenizer(str);
        System.out.println("----Split by space----");
        while (st.hasMoreElements()) {
            System.out.print(st.nextElement());
        }
    }

    /**
     *
     * @param str
     */
    public static  void  splitByComma(String str) {
        System.out.println("----Split by comma----");
        StringTokenizer stTwo = new StringTokenizer(str, ",");

        while (stTwo.hasMoreElements()) {
            System.out.print(stTwo.nextElement());
        }
    }
}
