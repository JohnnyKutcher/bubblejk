package trysomethingnew.strings;

/**
 * Created by Admin on 14.06.2017.
 */
public class SubString {
    /**
     *
     * @param str
     * @return
     */
    public static String subStringSearch(String str) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length() / 2; i++) {
            char symbol = str.charAt(i);
            if (symbol == str.charAt(str.length() - (i + 1))) {
                result.append(symbol);
            } else {
                break;
            }
        }
        return result.toString();
    }
}
