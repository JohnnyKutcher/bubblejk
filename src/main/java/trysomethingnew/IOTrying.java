package trysomethingnew;

import java.io.*;
import java.util.Arrays;

/**
 * Created by Admin on 13.06.2017.
 */
public class IOTrying {
    /**
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        File file = new File("text.txt");
        FileWriter fw = new FileWriter(file);

        CharSequence charSequence = "SVN uses a single central repository to serve as the communication hub "
                + "\nfor developers, and collaboration takes place by passing changesets between the developers’ "
                + "\nworking copies and the central repository. This is different from Git’s collaboration model, "
                + "\nwhich gives every developer their own copy of the repository, complete with its own local "
                + "\nhistory and branch structure. Users typically need to share a series of commits rather than "
                + "\na single changeset. Instead of committing a changeset from a working copy to the central "
                + "\nrepository, Git lets you share entire branches between repositories."
                + "\nThe commands presented below let you manage connections with other repositories, publish "
                + "\nlocal history by \"pushing\" branches to other repositories, and see what others have contributed "
                + "\nby \"pulling\" branches into your local repository.";
        fw.append(charSequence);
        fw.flush();
        fw.close();

        BufferedReader br = new BufferedReader(new FileReader(file));

        String line = "";
        int[] n = null;
        while ((line = br.readLine()) != null) {
            String[] str = line.split(" ");
            n = new int[str.length];
            for (int i = 0; i < str.length; i++) {
                n[i] = Integer.valueOf(str[i]);
                System.out.print(" Nm - " + n[i]);
            }
        }
        System.out.println();
        System.out.println(Arrays.toString(n));
        br.close();
    }
}
