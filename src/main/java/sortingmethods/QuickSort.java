package sortingmethods;

/**
 * Created by Admin on 09.06.2017.
 */
public class QuickSort {
    /**
     *
     * @param items
     */
    public static void qsort(int[] items) {
        qs(items, 0, items.length - 1);
    }

    private static void qs(int[] items, int left, int right) {
        int i;
        int j;
        int x;
        int y;
        i = left;
        j = right;
        x = items[(left + right) / 2];

        do {
            while ((items[i] < x) && (i < right)) {
                i++;
            }
            while ((x < items[j]) && (j > left)) {
                j--;
            }


            if (i <= j) {
                y = items[i];
                items[i] = items[j];
                items[j] = y;
                i++;
                j--;
            }
        } while (i <= j);
        if (left < j) {
            qs(items, left, j);
        }
        if (i < right) {
            qs(items, i, right);
        }
    }
}
