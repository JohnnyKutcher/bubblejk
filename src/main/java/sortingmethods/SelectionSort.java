package sortingmethods;

/**
 * Created by Admin on 09.06.2017.
 */
public class SelectionSort {
    /**
     *
     * @param numbers
     */
    public static void selectionSort(int[] numbers) {
        int min;
        int temp;

        for (int index = 0; index < numbers.length - 1; index++) {
            min = index;
            for (int scan = index + 1; scan < numbers.length; scan++) {
                if (numbers[scan] < numbers[min]) {
                    min = scan;
            }
        }
            temp = numbers[min];
            numbers[min] = numbers[index];
            numbers[index] = temp;
        }
    }
}
