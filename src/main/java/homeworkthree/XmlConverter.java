package homeworkthree;

import java.io.*;

/**
 * Created by Admin on 14.06.2017.
 */
public class XmlConverter {
    /**
     * read data from an xml files to a string
     * @param filename
     * @return
     */
    public static String lineXml(String filename) {
        String xmlContent = "";
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                xmlContent += line;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return xmlContent;
    }

    /**
     * remove all tags from string and write data to a new text file
     * @param xmlSource
     */
    public static void newFileWOTags(String xmlSource) {
        File file = new File("new-file.txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(xmlSource.replaceAll("<[^>]*>", "")
                    .replaceAll("\\s{2,}", "\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * replace all tags and write data to a text file
     * @param xmlSource
     */
    public static void replaceTags(String xmlSource) {
        File file = new File("file-with-new-tags.txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(xmlSource.replaceAll("<[^>]*>", "<tag>")
                    .replaceAll("\\s{2,}", "\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
