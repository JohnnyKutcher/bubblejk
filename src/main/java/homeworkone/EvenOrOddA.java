package homeworkone;

/**
 * Created by Admin on 09.06.2017.
 */
public class EvenOrOddA {
    protected EvenOrOddA() {
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static int multiplyAB(int a, int b) {
        int mult;
        int add;
        if ((a % 2) == 0) {
            mult = a * b;
            return mult;
        } else {
            add = a + b;
            return add;
        }
    }
}
