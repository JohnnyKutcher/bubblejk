package homeworkone;

/**
 * Created by Admin on 09.06.2017.
 */
public class SortOfValues {
    protected SortOfValues() {
    }

    /**
     *
     * @param a
     * @param b
     */
    public static void sortOfAB(int a, int b) {
        if (a > b) {
            System.out.println(b + " " + a);
        } else {
            System.out.println(a + " " + b);
        }
}

    /**
     *
     * @param a
     * @param b
     * @param c
     */
    public static void sortOfABC(int a, int b, int c) {
        if (a > b && a > c) {
            if (c > b) {
                System.out.println(b + " " + c + " " + a);
            } else {
                System.out.println(c + " " + b + " " + a);
            }
        } else if (b > a && b > c) {
            if (a > c) {
                System.out.println(c + " " + a + " " + b);
            } else {
                System.out.println(a + " " + c + " " + b);
        }
}
        if (c > a && c > b) {
            if (a > b) {
                System.out.println(b + " " + a + " " + c);
            } else {
                System.out.println(a + " " + b + " " + c);
            }
        }
    }

    /**
     *
     * @param a
     * @param b
     * @param c
     * @param d
     */
    public static void sortOfABCD(int a, int b, int c, int d) {
        if (a > b && b > c) {
            if (c > d) {
                System.out.println(d + " " + c + " " + b + " " + a);
            } else if (b > d) {
                System.out.println(c + " " + d + " " + b + " " + a);
            } else if (a > d) {
                System.out.println(c + " " + b + " " + d + " " + a);
            } else {
                System.out.println(c + " " + b + " " + a + " " + d);
            }
        }
        if (b > a && a > c) {
            if (c > d) {
                System.out.println(d + " " + c + " " + a + " " + b);
            } else if (a > d) {
                System.out.println(c + " " + d + " " + a + " " + b);
            } else if (b > d) {
                System.out.println(c + " " + a + " " + d + " " + b);
            } else {
                System.out.println(c + " " + a + " " + b + " " + d);
            }
        }
        if (c > b && b > a) {
            if (a > d) {
                System.out.println(d + " " + a + " " + b + " " + c);
            } else if (b > d) {
                System.out.println(a + " " + d + " " + b + " " + c);
            } else if (c > d) {
                System.out.println(a + " " + b + " " + d + " " + c);
            } else {
                System.out.println(a + " " + b + " " + c + " " + d);
            }
        }
        if (a > c && c > b) {
            if (b > d) {
                System.out.println(d + " " + b + " " + c + " " + a);
            } else if (c > d) {
                System.out.println(b + " " + d + " " + c + " " + a);
            } else if (a > d) {
                System.out.println(b + " " + c + " " + d + " " + a);
            } else {
                System.out.println(b + " " + c + " " + a + " " + d);
            }
        }
        if (b > c && c > a) {
            if (a > d) {
                System.out.println(d + " " + a + " " + c + " " + b);
            } else if (c > d) {
                System.out.println(a + " " + d + " " + c + " " + b);
            } else if (b > d) {
                System.out.println(a + " " + c + " " + d + " " + b);
            } else {
                System.out.println(a + " " + c + " " + b + " " + d);
            }
        }
        if (c > a && a > b) {

            if (b > d) {
                System.out.println(d + " " + b + " " + a + " " + c);
            } else if (a > d) {
                System.out.println(b + " " + d + " " + a + " " + c);
            } else if (c > d) {
                System.out.println(b + " " + a + " " + d + " " + c);
            } else {
                System.out.println(b + " " + a + " " + c + " " + d);
            }
        }
    }

}
