package homeworkone;

/**
 * Created by Admin on 09.06.2017.
 */
public class BiggerValue {
    protected BiggerValue() {
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static int checkTheBiggerValueBetweenAOrB(int a, int b) {

        if (a > b) {
            return a;
        } else {
            return b;
        }
}

    /**
     *
     * @param a
     * @param b
     * @param c
     * @return
     */
    public static int checkTheBiggerValueBetweenAOrBOrC(int a, int b, int c) {
        if (a > b && a > c) {
            return a;
        } else if (b > a && b > c) {
            return b;
        } else {
            return c;
        }
    }

    /**
     *
     * @param a
     * @param b
     * @param c
     * @param d
     * @return
     */
    public static int checkTheBiggerValueBetweenAOrBOrCOrD(int a, int b, int c, int d) {
        if (a > b && a > c && a > d) {
            return a;
        } else if (b > a && b > c && b > d) {
            return b;
        } else if (c > a && c > b && c > d) {
            return c;
        } else {
            return d;
        }
    }
}
