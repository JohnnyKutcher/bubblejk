package homeworktwo;

import java.util.Arrays;

/**
 * Created by Admin on 07.06.2017.
 */
public class ArraysSortingMethod {
    /**
     *
     * @param a
     */
    public static void sortingByArraysMethod(int[] a) {
        Arrays.sort(a);
        System.out.println(Arrays.toString(a));
    }
}
