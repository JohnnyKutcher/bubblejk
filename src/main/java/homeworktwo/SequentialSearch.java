package homeworktwo;

/**
 * Created by Admin on 07.06.2017.
 */
public class SequentialSearch {
    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static boolean contains(int[] a, int b) {
        for (int i : a) {
            if (i == b) {
                return true;
            }
        }
        return false;
    }
}

