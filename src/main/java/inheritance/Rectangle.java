package inheritance;

/**
 * Created by Admin on 12.06.2017.
 */
public class Rectangle extends BaseForFigures {
    /**
     * creating of a class for Rectangle, that extends parent class
     */
    public Rectangle() {
        super();
    }

    public Rectangle(double height, double width) {
        super(height, width);
    }

    public Rectangle(double x) {
        super(x);
    }

    /**
     *
     */
    public void square() {
        if (getHeight() == getWidth()) {
            System.out.println("We got a square.");
        } else {
            System.out.println("Our figure is rectangle");
        }
    }

    @Override
    public double area() {
        return getWidth() * getHeight();
    }

    @Override
    public String toString() {
        if (getHeight() == getWidth()) {
            return "Our figure is Square." + " The side of the square is: " + getHeight()
                    + ", so the area is: " + area();
        } else {
            return "Our figure is Rectangle." + " The height of the rectangle is: " + getHeight()
                    + ", and the width is: " + getWidth() + ", so the area is: " + area();
        }
    }
}