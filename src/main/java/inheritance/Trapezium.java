package inheritance;

/**
 * Created by Admin on 12.06.2017.
 */
public class Trapezium extends BaseForFigures {
   /**
   * declaration of upper side variable
   */
   private double upperSide;

   /**
     * declaration of bottom side variable
     */
   private double bottomSide;

    public Trapezium(double upperSide) {
        super();
        this.upperSide = upperSide;
        this.bottomSide = getWidth();
    }

    public Trapezium(double x, double upperSide) {
        super(x);
        this.upperSide = upperSide;
        this.bottomSide = getWidth();
    }

    public Trapezium(double height, double width, double upperSide) {
        super(height, width);
        this.upperSide = upperSide;
        this.bottomSide = getWidth();
    }

    public double getUpperSide() {
        return upperSide;
    }

    public void setUpperSide(double upperSide) {
        this.upperSide = upperSide;
    }

    public double getBottomSide() {
        return bottomSide;
    }

    public void setBottomSide(double bottomSide) {
        this.bottomSide = bottomSide;
    }

    @Override
    public double area() {
        return ((getUpperSide() * getBottomSide()) / 2) * getHeight();
    }

    @Override
    public String toString() {
        return "Our figure is Trapezium." + " The upper side of the trapezium is: " + getUpperSide()
                + ", and the bottom side is: " +  getBottomSide() + ", so the area is: " + area();
    }
}
