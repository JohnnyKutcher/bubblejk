package inheritance;

/**
 * Created by Admin on 12.06.2017.
 */
public class Cylinder extends Circle implements ThreeDimFigures {
    /**
     * creating of a class for Cylinder, that extends parent class
     */
    public Cylinder() {
        super();
    }

    public Cylinder(double x) {
        super(x);
    }

    @Override
    public double area() {
        return Math.PI * Math.pow(getRadius(), 2.0);
    }

    @Override
    public double getVolume() {
        return area() * getHeight();
    }

    @Override
    public double getFullArea() {
        return 2 * area() + 2 * Math.PI * getHeight();
    }

    @Override
    public String toString() {
        return "Our figure is Cylinder." + "\nThe radius of the cylinder is: " + getRadius()
                + "\nThe full area of the cylinder is: " + getFullArea()
                + "\nThe volume of the cylinder is: " + getVolume();
    }
}
