package inheritance;

/**
 * Created by Admin on 12.06.2017.
 */
abstract class BaseForFigures {
    /**
     * declaration of a height of the figure
     */
    private double height;
    /**
     *  declaration of a width of the figure
     */
    private double width;

    protected BaseForFigures() {
        width = height = 1.0;
    }

    protected BaseForFigures(double height, double width) {
        this.height = height;
        this.width = width;
    }

    protected BaseForFigures(double x) {
        width = height = x;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    abstract double area();
}
