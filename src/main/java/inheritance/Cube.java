package inheritance;

/**
 * Created by Admin on 12.06.2017.
 */
public class Cube extends BaseForFigures implements ThreeDimFigures {
    /**
     * creating of a class for Cube, that extends parent class
     */
    public Cube() {
        super();
    }

    public Cube(double x) {
        super(x);
    }

    @Override
    public double area() {
        return Math.pow(getHeight(), 2.0);
    }

    @Override
    public double getFullArea() {
        return 6 * area();
    }

    @Override
    public double getVolume() {
        return Math.pow(getHeight(), 3.0);
    }

    @Override
    public String toString() {
        return "Our figure is Cube." + "\nThe side of the cube is: " + getHeight()
                + "\nThe full area of the cube is: " + getFullArea()
                + "\nThe volume of the cube is: " + getVolume();
    }
}
