package inheritance;

/**
 * Created by Admin on 12.06.2017.
 */
public class Triangle extends BaseForFigures {
    /**
     * creating of a class for Triangle, that extends parent class
     */
    public Triangle() {
        super();
    }

    public Triangle(double height, double width) {
        super(height, width);
    }

    public Triangle(double x) {
        super(x);
    }

    @Override
    public double area() {
        return getHeight() * getWidth() / 2;
    }

    @Override
    public String toString() {
        return "Our figure is Triangle." + " The height of the triangle is: " + getHeight() + ", and the width is: "
                + getWidth() + ", so the area is: " + area();
    }
}
