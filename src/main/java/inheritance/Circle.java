package inheritance;

/**
 * Created by Admin on 12.06.2017.
 */
public class Circle extends BaseForFigures {
    /**
     * creating of a class for Circle, that extends parent class
     */
    public Circle() {
        super();
    }

    public Circle(double x) {
        super(x);
    }

    public double getRadius() {
        return getHeight() / 2;
    }

    /**
     *
     * @return
     */
    @Override
    public double area() {
        return getRadius() * getRadius() * Math.PI;
    }

    @Override
    public String toString() {
        return "Our figure is Cylinder." + " The radius of the circle is: " + getRadius()
                + ", and the area is: " + area();
    }
}
