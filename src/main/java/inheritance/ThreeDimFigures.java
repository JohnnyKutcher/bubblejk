package inheritance;

/**
 * Created by Admin on 12.06.2017.
 */
public interface ThreeDimFigures {
    /**
     * method for volume of three dimensional figures
     * @return
     */
    double getVolume();

    /**
     * method for full area of three dimensional figures
     * @return
     */
    double getFullArea();
}
