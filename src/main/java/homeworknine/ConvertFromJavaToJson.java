package homeworknine;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Admin on 26.07.2017.
 */
public class ConvertFromJavaToJson {

    /**
     *
     * @param newPath
     * @param type
     * @param name
     * @param age
     * @param position
     * @param salary
     * @param skills
     */
    public static void convertingJavaObjToJson(String newPath, Employee type, String name, Integer age,
                                               String position, Integer salary, List<String> skills) {

        ObjectMapper objectMapper = new ObjectMapper();

        type = createJson(type, name, age, position, salary, skills);

        try {
            objectMapper.writeValue(
                    new File(newPath), type);

            String jsonInString = objectMapper.writeValueAsString(type);
            System.out.println(jsonInString);

            jsonInString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(type);
            System.out.println(jsonInString);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * @param newEmployee
     * @param name
     * @param age
     * @param position
     * @param salary
     * @param skills
     * @return
     */
    public static Employee createJson(Employee newEmployee, String name, Integer age,
                                      String position, Integer salary, List<String> skills) {
        newEmployee.setName(name);
        newEmployee.setAge(age);
        newEmployee.setPosition(position);
        newEmployee.setSalary(new BigDecimal(salary));
        newEmployee.setSkills(skills);

        return newEmployee;
    }
}
