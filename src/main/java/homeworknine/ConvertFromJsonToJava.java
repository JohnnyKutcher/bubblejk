package homeworknine;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

//import org.apache.poi.ss.formula.functions.T;

/**
 * Created by Admin on 26.07.2017.
 */
public class ConvertFromJsonToJava {

    /**
     *
     * @param filePath
     * @param type
     */
    public static  void convertFromJsonToJava(String filePath, Employee type) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            type = objectMapper.readValue(
                    new File(filePath), Employee.class);

            System.out.println(objectMapper.writeValueAsString(type));

            String e = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(type);
            System.out.println(e);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
