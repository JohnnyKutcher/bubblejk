package homeworkfifteen;

import java.sql.*;
import java.text.ParseException;

/**
 * homework 15, import data from CSV-file to DB
 */
public class FromCSVToDB {

    /**
     * main method
     * @param args
     */
    public static void main(String[] args) {
        try {
            connect("jdbc:mysql://localhost:3306/company", "root", "admin",
                    "/home/ivan/IdeaProjects/Ivan/src/main/resources/csv/DBase.csv");

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * method for establishing connection
     * @param url
     * @param user
     * @param password
     * @param fileName
     * @return
     * @throws ParseException
     */
    public static Connection connect(String url, String user, String password, String fileName)
            throws ParseException {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            createDBUserTable(connection);
            importDataIntoDB(connection, fileName);
            return connection;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * method for creating new DataBase
     * @param connection
     * @throws SQLException
     * @throws ParseException
     */
    public static void createDBUserTable(Connection connection) throws SQLException, ParseException {

        String createTableSQL = "CREATE TABLE IF NOT EXISTS company(ID INT PRIMARY KEY AUTO_INCREMENT,"
                + "name VARCHAR(55) NOT NULL,"
                + "age INTEGER NOT NULL,"
                + "salary INTEGER NOT NULL"
                + ")";

        try (Statement statement = connection.createStatement()) {
            statement.execute(createTableSQL);

            System.out.println("Table  \"new company\" is created!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * method for import data from CSV-file to DataBase using MySQL
     * @param connection
     * @param fileName
     * @throws ParseException
     */
    private static void importDataIntoDB(Connection connection, String fileName) throws ParseException {
        String importFromCSV = "LOAD DATA LOCAL INFILE '" + fileName + "' INTO TABLE company "
                + "FIELDS TERMINATED BY ',' LINES TERMINATED BY '\\n' IGNORE 1 ROWS;";

        try (Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE)) {

            statement.executeUpdate(importFromCSV);


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
