/**
 * DynamicReports - Free Java reporting library for creating reports dynamically
 *
 * Copyright (C) 2010 - 2016 Ricardo Mariaca
 * http://www.dynamicreports.org
 *
 * This file is part of DynamicReports.
 *
 * DynamicReports is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DynamicReports is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DynamicReports. If not, see <http://www.gnu.org/licenses/>.
 */

package homeworkfive.complex.dynamicreport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ricardo Mariaca (r.mariaca@dynamicreports.org)
 */
public class DynamicReport {
	/**
	 *
	 */
	private String title;
	/**
	 *
	 */
	private List<DynamicColumn> columns;
	/**
	 *
	 */
	private List<String> groups;
	/**
	 *
	 */
	private List<String> subtotals;
	/**
	 *
	 */
	private boolean showPageNumber;

	public DynamicReport() {
		columns = new ArrayList<DynamicColumn>();
		groups = new ArrayList<String>();
		subtotals = new ArrayList<String>();
	}

	/**
	 *
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 *
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 *
	 * @return
	 */
	public List<DynamicColumn> getColumns() {
		return columns;
	}

	/**
	 *
	 * @param columns
	 */
	public void setColumns(List<DynamicColumn> columns) {
		this.columns = columns;
	}

	/**
	 *
	 * @param column
	 */
	public void addColumn(DynamicColumn column) {
		this.columns.add(column);
	}

	/**
	 *
	 * @return
	 */
	public List<String> getGroups() {
		return groups;
	}

	/**
	 *
	 * @param groups
	 */
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

	/**
	 *
	 * @param column
	 */
	public void addGroup(String column) {
		this.groups.add(column);
	}

	/**
	 *
	 * @return
	 */
	public List<String> getSubtotals() {
		return subtotals;
	}

	/**
	 *
	 * @param subtotals
	 */
	public void setSubtotals(List<String> subtotals) {
		this.subtotals = subtotals;
	}

	/**
	 *
	 * @param column
	 */
	public void addSubtotal(String column) {
		this.subtotals.add(column);
	}

	/**
	 *
	 * @return
	 */
	public boolean isShowPageNumber() {
		return showPageNumber;
	}

	/**
	 *
	 * @param showPageNumber
	 */
	public void setShowPageNumber(boolean showPageNumber) {
		this.showPageNumber = showPageNumber;
	}

}
