/**
 * DynamicReports - Free Java reporting library for creating reports dynamically
 *
 * Copyright (C) 2010 - 2016 Ricardo Mariaca
 * http://www.dynamicreports.org
 *
 * This file is part of DynamicReports.
 *
 * DynamicReports is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DynamicReports is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DynamicReports. If not, see <http://www.gnu.org/licenses/>.
 */

package homeworkfive.complex.invoice;

import homeworkfive.Templates;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.base.expression.AbstractSimpleExpression;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.component.HorizontalListBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.builder.subtotal.AggregationSubtotalBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.definition.ReportParameters;
import net.sf.dynamicreports.report.exception.DRException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * @author Ricardo Mariaca (r.mariaca@dynamicreports.org)
 */
public class InvoiceDesign {
	/**
	 *
	 */
	private InvoiceData data = new InvoiceData();
	/**
	 *
	 */
	private AggregationSubtotalBuilder<BigDecimal> totalSum;

	/**
	 *
	 * @return
	 * @throws DRException
	 */
	public JasperReportBuilder build() throws DRException {
		JasperReportBuilder report = report();

		//init styles
		StyleBuilder columnStyle = stl.style(Templates.columnStyle)
			.setBorder(stl.pen1Point());
		StyleBuilder subtotalStyle = stl.style(columnStyle)
			.bold();
		StyleBuilder shippingStyle = stl.style(Templates.boldStyle)
			.setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);

		//init columns
		TextColumnBuilder<Integer> rowNumberColumn = col.column("#", "number", type.integerType())
			.setFixedWidth(20)
			.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
		TextColumnBuilder<String> descriptionColumn = col.column("Description", "description", type.stringType())
			.setFixedWidth(250);
		TextColumnBuilder<Integer> quantityColumn = col.column("Quantity", "quantity", type.integerType())
				.setFixedWidth(50).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
		TextColumnBuilder<BigDecimal> unitPriceColumn = col.column("Unit Price", "unitprice", Templates.currencyType);
		TextColumnBuilder<String> taxColumn = col.column("Tax", exp.text("20%"))
			.setFixedColumns(3);
		//price = unitPrice * quantity
		TextColumnBuilder<BigDecimal> priceColumn = unitPriceColumn.multiply(quantityColumn)
			.setTitle("Price")
			.setDataType(Templates.currencyType);
		//vat = price * tax
		TextColumnBuilder<BigDecimal> vatColumn = priceColumn.multiply(data.getInvoice().getTax())
			.setTitle("VAT")
			.setDataType(Templates.currencyType);
		//total = price + vat
		TextColumnBuilder<BigDecimal> totalColumn = priceColumn.add(vatColumn)
			.setTitle("Total Price")
			.setDataType(Templates.currencyType)
			.setRows(2)
			.setStyle(subtotalStyle);
		//init subtotals
		totalSum = sbt.sum(totalColumn)
			.setLabel("Total:")
			.setLabelStyle(Templates.boldStyle);

		//configure report
		report
			.setTemplate(Templates.reportTemplate)
			.setColumnStyle(columnStyle)
			.setSubtotalStyle(subtotalStyle)
			//columns
			.columns(
				rowNumberColumn, descriptionColumn, quantityColumn, unitPriceColumn, totalColumn, priceColumn, taxColumn, vatColumn)
			.columnGrid(
				rowNumberColumn, descriptionColumn, quantityColumn, unitPriceColumn,
				grid.horizontalColumnGridList()
					.add(totalColumn).newRow()
					.add(priceColumn, taxColumn, vatColumn))
			//subtotals
			.subtotalsAtSummary(
				totalSum, sbt.sum(priceColumn), sbt.sum(vatColumn))
			//band components
			.title(
				Templates.createTitleComponent("Invoice No.: " + data.getInvoice().getId(),
						data.getInvoice().getDate()),
					cmp.verticalList(
					cmp.vListCell((createCustomerComponent("Supplier", data.getInvoice().getSupplier()))),
					cmp.vListCell((createCustomerComponent("Buyer", data.getInvoice().getBuyer()))))
							.setStyle(stl.style(10)).setGap(10),
				cmp.verticalGap(10))
			.pageFooter(
				Templates.footerComponent)
			.summary(
				cmp.horizontalList(
					cmp.text(new TotalPaymentExpression()).setStyle(Templates.bold12CenteredStyle).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT))
						.newRow()
						.add(cmp.verticalGap(30))
						.newRow()
						.add(cmp.line())
						.newRow().add(cmp.verticalGap(15)),
				cmp.verticalList(
						cmp.text("Wrote out:      _________________________").setStyle(Templates.bold12CenteredStyle)
						.setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT),
						cmp.text("\n\n\nAttention! THE BILL IS VALID ONLY FOR THREE DAYS!"
								+ "\nATTENTION! in the destination of the payment, "
								+ "\nbe sure to indicate the invoice number"
								+ "\nFor quick receipt of goods, please, send copies of documents."
								+ "\nFor legal entities: power of attorney and copy of the certificate."
								+ "\nSend documents to e-mail: doc@rozetka.ua or fax (044)5029930"
								+ "\nIn case of non-observance of these conditions, payment will not be counted!")
								.setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)
								.setStyle(stl.style(Templates.boldStyle))))
			.setDataSource(data.createDataSource());

		return report;
	}

	/**
	 *
	 * @param label
	 * @param customer
	 * @return
	 */
	private ComponentBuilder<?, ?> createCustomerComponent(String label, Customer customer) {
		HorizontalListBuilder list = cmp.horizontalList().setBaseStyle(stl.style()
				.setTopBorder(stl.pen1Point()).setLeftPadding(10));
		addCustomerAttribute(list, "Company Name", customer.getCompanyName());
		addCustomerAttribute(list, "Requisites", customer.getRequisites());
			return cmp.verticalList(
							cmp.text(label).setStyle(Templates.boldStyle).setStyle(Templates.bold12CenteredStyle)
									.setHorizontalTextAlignment(HorizontalTextAlignment.LEFT), list);
	}

	/**
	 *
	 * @param list
	 * @param label
	 * @param value
	 */
	private void addCustomerAttribute(HorizontalListBuilder list, String label, String value) {
		if (value != null) {
			list.add(cmp.text(label + ":").setFixedColumns(8).setStyle(Templates.boldStyle).setFixedWidth(100), cmp.text(value)).newRow();
		}
	}

	/**
	 *
	 */
	private class TotalPaymentExpression extends AbstractSimpleExpression<String> {
		private static final long serialVersionUID = 1L;

		@Override
		public String evaluate(ReportParameters reportParameters) {
			BigDecimal total = reportParameters.getValue(totalSum);
			return "Total payment: " + Templates.currencyType.valueToString(total, reportParameters.getLocale());
		}
	}

	/**
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		InvoiceDesign design = new InvoiceDesign();
		try {
			JasperReportBuilder report = design.build();
			report.toPdf(new FileOutputStream(new File("C:\\Users\\Admin\\IdeaProjects\\apollo\\my_invoice.pdf")));
		} catch (DRException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}