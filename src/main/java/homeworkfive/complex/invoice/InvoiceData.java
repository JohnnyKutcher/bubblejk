/**
 * DynamicReports - Free Java reporting library for creating reports dynamically
 *
 * Copyright (C) 2010 - 2016 Ricardo Mariaca
 * http://www.dynamicreports.org
 *
 * This file is part of DynamicReports.
 *
 * DynamicReports is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DynamicReports is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DynamicReports. If not, see <http://www.gnu.org/licenses/>.
 */

package homeworkfive.complex.invoice;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Ricardo Mariaca (r.mariaca@dynamicreports.org)
 */
public class InvoiceData {
	/**
	 *
	 */
	private Invoice invoice;
	/**
	 *
	 */
	Date date = new Date();
	/**
	 *
	 */
	String str = String.format("%1$s %2$td %2$tm %2$tY", "Bill Date: ", date);

	public InvoiceData() {
		invoice = createInvoice();
	}

	/**
	 *
	 * @return
	 */
	private Invoice createInvoice() {
		Invoice invoice = new Invoice();
		invoice.setId("CF-01121439");
		invoice.setDate(str);
		invoice.setTax(0.2);

		invoice.setSupplier(createCustomer("LLC ROZETKA.UA", "s/a 2600228536520, Bank JSC 'UkrSibBank', Harkiv, "
				+ "MFO 351005," +  "\n01103, Kiev, boulevard Drugby Narodov, 8-A, phone: 0445029931, "
				+ "\nEDRPOU 379193071, Certificate number 100291522"));
		invoice.setBuyer(createCustomer("LLC TRANSGROUP", null));

		List<Item> items = new ArrayList<Item>();
		items.add(createItem(1, "Keyboard Microsoft Wired Keyboard 600 USB Port Russian Hdwr Black"
				+ "\n(ANB-00018)", 1, new BigDecimal(12.54)));
		items.add(createItem(2, "Organization of dilivery in the city of Kiev",
				1, new BigDecimal(1.11)));
		invoice.setItems(items);

		return invoice;
	}

	/**
	 *
	 * @param name
	 * @param req
	 * @return
	 */
	private Customer createCustomer(String name, String req) {
		Customer customer = new Customer();
		customer.setCompanyName(name);
		customer.setRequisites(req);
		return customer;
	}

	/**
	 *
	 * @param number
	 * @param description
	 * @param quantity
	 * @param unitprice
	 * @return
	 */
	private Item createItem(Integer number, String description, Integer quantity, BigDecimal unitprice) {
		Item item = new Item();
		item.setNumber(number);
		item.setDescription(description);
		item.setQuantity(quantity);
		item.setUnitprice(unitprice);
		return item;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	/**
	 *
	 * @return
	 */
	public JRDataSource createDataSource() {
		return new JRBeanCollectionDataSource(invoice.getItems());
	}
}
