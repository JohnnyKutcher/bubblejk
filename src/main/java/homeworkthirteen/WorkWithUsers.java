package homeworkthirteen;

import net.sf.dynamicreports.adhoc.AdhocManager;
import net.sf.dynamicreports.adhoc.configuration.AdhocColumn;
import net.sf.dynamicreports.adhoc.configuration.AdhocConfiguration;
import net.sf.dynamicreports.adhoc.configuration.AdhocReport;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Admin on 01.09.2017.
 */
public class WorkWithUsers {
    /**
     * main method
     * @param args
     */
    public static void main(String[] args) {

        String[] user1 = {"1", "Ivan", "junior", "300"};
        String[] user2 = {"2", "Alexander", "junior", "300"};
        String[] user3 = {"3", "Igor", "junior", "300"};
        String[] user4 = {"4", "Nikolai", "junior", "300"};
        String[] user5 = {"5", "Bogdan", "junior", "300"};
        String[] user6 = {"6", "Anton", "junior", "300"};
        String[] user7 = {"7", "Taras", "junior", "300"};
        String[] user8 = {"8", "Sarybeg", "junior", "300"};
        String[] user9 = {"9", "Aleksey", "senior", "1200"};
        String[] user10 = {"10", "Grisha", "middle", "800"};

        ExecutorService service = Executors.newWorkStealingPool();

        List<Callable<String>> callables = Arrays.asList(
                () -> build(setDataToUserClass(user1)),
                () -> build(setDataToUserClass(user2)),
                () -> build(setDataToUserClass(user3)),
                () -> build(setDataToUserClass(user4)),
                () -> build(setDataToUserClass(user5)),
                () -> build(setDataToUserClass(user6)),
                () -> build(setDataToUserClass(user7)),
                () -> build(setDataToUserClass(user8)),
                () -> build(setDataToUserClass(user9)),
                () -> build(setDataToUserClass(user10))
        );

        try {
            service.invokeAll(callables).stream().map(future -> {
                try {
                    return future.get();
                } catch (Exception e) {
                    throw new IllegalStateException(e);
                }
            }).forEach(System.out::println);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * method for setting data to User
     * @param userData
     * @return
     */
    public static User setDataToUserClass(String[] userData) {
        User user = null;
        for (int i = 0; i < userData.length; i++) {
            user = new User(Integer.parseInt(userData[0]), userData[1], userData[2], Integer.parseInt(userData[3]));
        }
        return user;
    }

    private static String build(User user) {

        AdhocConfiguration configuration = new AdhocConfiguration();
        AdhocReport report = new AdhocReport();
        configuration.setReport(report);

        AdhocColumn column = new AdhocColumn();
        column.setName("id");
        report.addColumn(column);

        column = new AdhocColumn();
        column.setName("Name");
        report.addColumn(column);

        column = new AdhocColumn();
        column.setName("Position");
        report.addColumn(column);

        column = new AdhocColumn();
        column.setName("Salary");
        report.addColumn(column);

        try {
            //The following code stores the configuration to an xml file
            AdhocManager.saveConfiguration(configuration,
                    new FileOutputStream(
                            "C:\\Users\\Admin\\IdeaProjects\\ivanWork\\src\\main\\resources"
                                    + "\\xml\\configuration.xml"));
            @SuppressWarnings("unused")
            //The following code loads a configuration from an xml file
                    AdhocConfiguration loadedConfiguration = AdhocManager
                    .loadConfiguration(new FileInputStream(
                            "C:\\Users\\Admin\\IdeaProjects\\ivanWork\\src\\main\\resources"
                                    + "\\xml\\configuration.xml"));

            JasperReportBuilder reportBuilder = AdhocManager.createReport(configuration.getReport());
            reportBuilder.setDataSource(createDataSource(user));
            File file = new File("C:\\Users\\Admin\\IdeaProjects\\ivanWork\\" + user.getName());
            file.mkdir();
            reportBuilder.toPdf(new FileOutputStream(new File("C:\\Users\\Admin\\IdeaProjects\\ivanWork\\"
                    + user.getName() + "\\" + user.getName() + ".pdf")));
        } catch (DRException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return user.toString();
    }

    private static JRDataSource createDataSource(User user) {
        DRDataSource dataSource = new DRDataSource("id", "Name", "Position", "Salary");
        dataSource.add(user.getId(), user.getName(), user.getPosition(), user.getSalary());
        return dataSource;
    }
}
