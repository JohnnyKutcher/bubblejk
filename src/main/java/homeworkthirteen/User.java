package homeworkthirteen;

/**
 * Class that contain main information about user
 */
public class User {

    /**
     * User's id
     */
    private int id;

    /**
     * Name of User
     */
    private String name;

    /**
     * Position of User
     */
    private String position;

    /**
     * User's salary
     */
    private int salary;

    public User(int id, String name, String position, int salary) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "User{"
                + "name='" + name + '\''
                + ", position='" + position + '\''
                + ", salary=" + salary
                + '}';
    }
}
