package serilization;

import org.junit.Test;
import homeworknine.ConvertFromJavaToJson;
import homeworknine.ConvertFromJsonToJava;
import homeworknine.Employee;

import java.util.Arrays;

/**
 * Created by Admin on 25.07.2017.
 */
public class JsonTest {

    @Test
    public void test() {
        String path = "Employee.json";
        Employee employee = new Employee();
        ConvertFromJsonToJava.convertFromJsonToJava(path, employee);
    }

    @Test
    public void test1() {
        String path = "Employee.json";
        Employee employee = new Employee();

        ConvertFromJavaToJson.convertingJavaObjToJson(path, employee, "Ivan Kucher", 30,
                "Developer", 2500, Arrays.asList("java", "html", "css"));
    }
}
