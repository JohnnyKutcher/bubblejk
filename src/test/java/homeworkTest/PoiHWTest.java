package homeworkTest;

import homeworkeigth.NewHWExcel;
import org.junit.Test;

public class PoiHWTest {
    @Test
    public void test() {
        String[] sheets = {"Ivan", "Anton", "Oleksander", "Aleksey", "Nikolai", "Igor", "Saribeg",
                                "Nikita", "Taras", "Dmytriy"};
        String[] header = {"MONTH", "PAYCHECK"};
        Double[] salaryIvan = {400.00, 600.25, 528.36, 323.00, 421.20, 526.45, 623.80, 325.02,
        413.00, 215.30, 412.00, 400.00};
        Double[] salaryAnton = {333.00, 680.25, 512.36, 313.00, 421.20, 526.45, 623.80, 325.02,
        413.00, 215.30, 412.00, 400.00};
        Double[] salaryOleksander = {401.00, 690.25, 578.36, 223.00, 421.20, 526.45, 623.80, 325.02,
        413.00, 215.30, 412.00, 400.00};
        Double[] salaryAleksey = {4000.00, 6000.25, 5208.36, 3203.00, 4210.20, 5206.45, 6023.80, 3025.02,
        4103.00, 2105.30, 4102.00, 4000.00};
        Double[] salaryNikolai = {131.00, 600.25, 528.36, 393.00, 421.20, 526.45, 623.80, 325.02,
        413.00, 215.30, 412.00, 400.00};
        Double[] salaryIgor = {456.00, 600.25, 528.36, 323.00, 421.20, 526.45, 623.80, 325.02,
        413.00, 215.30, 4102.00, 400.00};
        Double[] salarySaribeg = {16.00, 600.25, 528.36, 323.00, 421.20, 526.45, 623.80, 325.02,
        413.00, 215.30, 412.00, 400.00};
        Double[] salaryNikita = {400.00, 600.25, 527.36, 323.00, 421.20, 526.45, 623.80, 325.02,
        413.00, 215.30, 412.00, 400.00};
        Double[] salaryTaras = {400.00, 600.25, 528.36, 323.00, 421.20, 526.45, 623.80, 325.02,
        413.00, 215.30, 412.00, 400.00};
        Double[] salaryDmytriy = {400.00, 400.25, 428.36, 423.00, 421.20, 526.45, 623.80, 325.02,
        413.00, 215.30, 412.00, 456.00};
        NewHWExcel newHWExcel = new NewHWExcel("Employee.xlsx", sheets);
        newHWExcel.createWorkbook(header, salaryIvan, salaryAnton, salaryOleksander, salaryAleksey, salaryNikolai,
        salaryIgor, salarySaribeg, salaryNikita, salaryTaras, salaryDmytriy);

    }
}
