package homeworkTest;

import org.junit.jupiter.api.Test;

import static homeworktwo.ArraysSortingMethod.sortingByArraysMethod;
import static homeworktwo.SequentialSearch.contains;

/**
 * Created by Admin on 07.06.2017.
 */
public class SecondHWTest {
    @Test
    public void arraysSortingTest() {
        int[] a = {545,9898,-64564,3,899,0,-564};
        sortingByArraysMethod(a);
    }
    @Test
    public void sequentialSearchTest() {
        int[] a = {-5,-4,-3,-2,-1,0,1,2,3,4,5};
        System.out.println(contains(a,5));
        System.out.println(contains(a, 8));
    }
}
