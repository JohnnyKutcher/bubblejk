package homeworkTest;


import org.junit.Test;

import java.util.Arrays;

import static sortingmethods.QuickSort.qsort;
import static sortingmethods.SelectionSort.selectionSort;
import static sortingmethods.ShellSort.shellsort;

/**
 * Created by Admin on 09.06.2017.
 */
public class SortingTest {

    @Test
    public void QSTest() {
        int[] a = new int[10];
        for (int i = 0; i < a.length; i++) {
            a[i] = (int) Math.round(Math.random() * 100);
            System.out.print(a[i] + " ");
        }
        System.out.println();
        qsort(a);
        System.out.println(Arrays.toString(a));
    }
    @Test
    public void SSTest() {
        int[] a = new int[10];
        for (int i = 0; i < a.length; i++) {
            a[i] = (int) Math.round(Math.random() * 100);
            System.out.print(a[i] + " ");
        }
        System.out.println();
        selectionSort(a);
        System.out.println(Arrays.toString(a));
    }
    @Test
    public void ShSTest() {
        Comparable[] a = new Comparable[10];
        for (int i = 0; i < a.length; i++) {
            a[i] = (int) Math.round(Math.random() * 100);
            System.out.print(a[i] + " ");
        }
        shellsort(a);
        System.out.println();
        System.out.println(Arrays.toString(a));
    }
}
