package homeworkTest;

import inheritance.*;
import org.junit.jupiter.api.Test;

/**
 * Created by Admin on 12.06.2017.
 */
public class Figures {
   @Test
    public void circleTest() {
       Circle circle = new Circle();
       System.out.println(circle.getRadius());
       System.out.println(circle.area());
       System.out.println(circle.toString());

       System.out.println();

       Circle circle1 = new Circle(7.9);
       System.out.println(circle1.getRadius());
       System.out.println(circle1.area());
       System.out.println(circle1.toString());
   }

   @Test
   public void cylinderTest() {
       Cylinder cylinder = new Cylinder();
       System.out.println(cylinder.getRadius());
       System.out.println(cylinder.area());
       System.out.println(cylinder.getFullArea());
       System.out.println(cylinder.getVolume());
       System.out.println(cylinder.toString());

       System.out.println();

       Cylinder cylinder1 = new Cylinder(12.65);
       System.out.println(cylinder1.getRadius());
       System.out.println(cylinder1.area());
       System.out.println(cylinder1.getFullArea());
       System.out.println(cylinder1.getVolume());
       System.out.println(cylinder1.toString());
   }

   @Test
    public void cubeTest() {
       Cube cube = new Cube();
       System.out.println(cube.getHeight());
       System.out.println(cube.area());
       System.out.println(cube.getFullArea());
       System.out.println(cube.getVolume());
       System.out.println(cube.toString());

       System.out.println();

       Cube cube1 = new Cube(15.3);
       System.out.println(cube1.getHeight());
       System.out.println(cube1.area());
       System.out.println(cube1.getFullArea());
       System.out.println(cube1.getVolume());
       System.out.println(cube1.toString());
   }
   @Test
    public void rectangleTest() {
       Rectangle rectangle = new Rectangle();
       System.out.println(rectangle.area());
       System.out.println(rectangle.getHeight());
       System.out.println(rectangle.toString());

       System.out.println();

       Rectangle rectangle1 = new Rectangle(5.6,7.3);
       System.out.println(rectangle1.area());
       System.out.println(rectangle1.getHeight());
       System.out.println(rectangle1.getWidth());
       System.out.println(rectangle1.toString());

       System.out.println();

       Rectangle rectangle2 = new Rectangle(8.4);
       System.out.println(rectangle2.area());
       System.out.println(rectangle2.getHeight());
       System.out.println(rectangle2.toString());
   }
   @Test
    public void triangleTest() {
       Triangle triangle = new Triangle();
       System.out.println(triangle.area());
       System.out.println(triangle.getHeight());
       System.out.println(triangle.getWidth());
       System.out.println(triangle.toString());

       System.out.println();

       Triangle triangle1 = new Triangle(6.2);
       System.out.println(triangle1.area());
       System.out.println(triangle1.getHeight());
       System.out.println(triangle1.getWidth());
       System.out.println(triangle1.toString());

       System.out.println();

       Triangle triangle2 = new Triangle(5.5, 7.1);
       System.out.println(triangle2.area());
       System.out.println(triangle2.getHeight());
       System.out.println(triangle2.getWidth());
       System.out.println(triangle2.toString());
   }
   @Test
   public void trapeziumTest() {
       Trapezium trapezium = new Trapezium(5.6);
       System.out.println(trapezium.getHeight());
       System.out.println(trapezium.getUpperSide());
       System.out.println(trapezium.getBottomSide());
       System.out.println(trapezium.area());
       System.out.println(trapezium.toString());

       System.out.println();

       Trapezium trapezium1 = new Trapezium(7.9,8.2);
       System.out.println(trapezium1.getHeight());
       System.out.println(trapezium1.getUpperSide());
       System.out.println(trapezium1.getBottomSide());
       System.out.println(trapezium1.area());
       System.out.println(trapezium1.toString());

       System.out.println();

       Trapezium trapezium2 = new Trapezium(5.4,7.2,8.3);
       System.out.println(trapezium2.getHeight());
       System.out.println(trapezium2.getUpperSide());
       System.out.println(trapezium2.getBottomSide());
       System.out.println(trapezium2.area());
       System.out.println(trapezium2.toString());
   }
}
