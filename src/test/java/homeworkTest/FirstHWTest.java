package homeworkTest;

import org.junit.jupiter.api.Test;

import static homeworkone.BiggerValue.checkTheBiggerValueBetweenAOrB;
import static homeworkone.BiggerValue.checkTheBiggerValueBetweenAOrBOrC;
import static homeworkone.BiggerValue.checkTheBiggerValueBetweenAOrBOrCOrD;
import static homeworkone.EvenOrOddA.multiplyAB;
import static homeworkone.SortOfValues.sortOfAB;
import static homeworkone.SortOfValues.sortOfABC;
import static homeworkone.SortOfValues.sortOfABCD;

/**
 * Created by Admin on 09.06.2017.
 */
public class FirstHWTest {
    @Test

    public void biggerValueTest() {
        System.out.println(checkTheBiggerValueBetweenAOrB(3,6));
        System.out.println(checkTheBiggerValueBetweenAOrBOrC(6,7,123));
        System.out.println(checkTheBiggerValueBetweenAOrBOrCOrD(1235,6584,1516,-1651));
    }
    @Test
    public void evenOrAddTest(){
        System.out.println(multiplyAB(459,32));
    }
    @Test
    public void sortOfValues() {
        sortOfAB(5689,23);
        sortOfABC(5646, 2, -5646);
        sortOfABCD(46454,0,-465416,132);
    }
}
