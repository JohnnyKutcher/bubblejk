package homeworkTest;



import org.junit.Test;

import static homeworkthree.XmlConverter.lineXml;
import static homeworkthree.XmlConverter.newFileWOTags;
import static homeworkthree.XmlConverter.replaceTags;

/**
 * Created by Admin on 14.06.2017.
 */
public class ConverterTest {
    @Test
    public void stringBldrFromXml() {
        String filename = "/home/ivan/IdeaProjects/Ivan/src/main/resources/xml/xmlHW.xml";
        System.out.println(lineXml(filename));
        newFileWOTags(lineXml(filename));
}

    @Test
    public void replaceAllTags() {
        String filename = "/home/ivan/IdeaProjects/Ivan/src/main/resources/xml/xmlHW.xml";
        replaceTags(lineXml(filename));
    }
}
