package homeworkten;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 03.08.2017.
 */
public class HWTest {

    @Test
    public void test() {
        String path = "text.txt";
        System.out.println(CountingOfWords.countWordsFromFile(path));
    }

    @Test
    public void testCsvFile() {

        String path = "C:\\Users\\Admin\\IdeaProjects\\apollo";
        String fileName = "file.csv";

        List<Person> personList = new ArrayList<>();
        personList.add(new Person(1,"ivan", 30));
        personList.add(new Person(2,"andrey", 26));
        personList.add(new Person(3,"igor", 33));
        personList.add(new Person(4,"anna", 25));
        personList.add(new Person(5,"tanya", 28));
        personList.add(new Person(6,"andrew", 24));
        personList.add(new Person(7,"carroll", 27));
        personList.add(new Person(8,"katy", 22));
        personList.add(new Person(9,"john", 31));
        personList.add(new Person(10,"juan", 35));

        CsvFileWriter.createCSVfile(path, fileName);
        CsvFileWriter.writeCsvFile("file.csv", personList);

        System.out.println(FromFileToObject.fromCsvFileToCollection(fileName));

        FromFileToObject.fromCollectionToObject(FromFileToObject.fromCsvFileToCollection(fileName));
    }
}
