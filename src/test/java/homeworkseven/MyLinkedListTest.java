package homeworkseven;

import org.junit.Test;

import java.util.Random;

/**
 * Created by Admin on 13.07.2017.
 */
public class MyLinkedListTest {
    @Test
    public void test1() {
        Random random = new Random();
        MyOwnLinkedList ll = new MyOwnLinkedList();
        for (int i = 0; i < 10; i++) {
            ll.add(random.nextInt(10));
        }

        ListIterator lIt = ll.iterator();
        while (lIt.hasNext()) {
            System.out.print(lIt.nextIndex());
            System.out.print(lIt.next());
            System.out.println();
        }

        System.out.println();

        while (lIt.hasPrevious()) {
            System.out.print(lIt.previous());
            System.out.print(lIt.previousIndex());
            System.out.println();
        }

        System.out.println(ll.subList(2,5));

        System.out.println(ll.indexOf(8));

        System.out.println(ll.lastIndexOf(8));

        ll.addLast(3);
        ll.addFirst(0);

        for (int i = 0; i < ll.size(); i++) {
            System.out.print(ll.get(i) + " ");
        }

        System.out.println("\n" + ll.getFirst() + "\n" + ll.getLast());

        ll.add(1,3);
        for (int i = 0; i < ll.size(); i++) {
            System.out.print(ll.get(i) + " ");
        }

        System.out.println();
        ll.set(0,5);
        for (int i = 0; i < ll.size(); i++) {
            System.out.print(ll.get(i) + " ");
        }
        System.out.println();
    }
}
