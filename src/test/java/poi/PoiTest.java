package poi;

import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Admin on 17.07.2017.
 */
public class PoiTest {
    @Test
    public void test() {
        ReadSheet readSheet = new ReadSheet();
        readSheet.createNewXlsx("C:\\Робота\\Робота\\Довідки\\Списання запчастин" +
                "\\32 КП Лєтранс ВЧД Шепетівка.xlsx");
    }

    @Test
    public void test1() {
        NewExcel newExcel = new NewExcel("Writesheet.xlsx");

        Map<String, Object[]> empinfo = new TreeMap<>();
        empinfo.put("1", new Object[] {"EMP ID", "EMP NAME", "DESIGNATION"});
        empinfo.put("2", new Object[] {"tp01", "Gopal", "Technical Manager"});
        empinfo.put("3", new Object[] {"tp02", "Manisha", "Proof Reader"});
        empinfo.put("4", new Object[] {"tp03", "Masthan", "Technical Writer"});
        empinfo.put("5", new Object[] {"tp04", "Satish", "Technical Writer"});
        empinfo.put("6", new Object[] {"tp05", "Krishna", "Technical Writer"});

        newExcel.createXlxs(empinfo, "Employee Info");
    }

    @Test
    public void test2() {
        String[] strings = {"Principal","Interest","Time","Output (P * r * t)"};
        Integer[] integers = {1, 1000, 12, 6};

        CalculateFormula calculateFormula = new CalculateFormula("Writesheet2.xlsx");
        calculateFormula.calculateFormulaMethod("Calculate Simple Interest",
                0, strings, 1, integers, "A2*B2*C2");
    }
}
